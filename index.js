const express = require("express");
const items = require("./person.json");
const fs = require("fs");

const app = express();

app.get("/", (req, res) => {
  res.render("index.ejs");
});

app.get("/item", (req, res) => {
  res.render("items.ejs", {
    data: items,
  });
});

fs.writeFileSync("./file.txt", "hy i love you");

app.listen(8000);
